"""
Bitmap At Corps
Bitmap At Corps is part of the Screen Font Pack

TODO: 
    Ramsay St also in bitmap 

Thom Janssen 2014

"""

from vanilla import * 
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from mojo.roboFont import CurrentFont, CurrentGlyph

f = CurrentFont()

margin = 7
atHeight = 22

def next(x):
	return x*21+7

def setGrid(grid):
	setDefault('glyphViewGridx', grid)
	setDefault('glyphViewGridy', grid)
	PostNotification("doodle.preferencesChanged")

def showGridAndBitmap(grid=True, bitmap=True):
	pref_as_is = getDefault('glyphZoomViewShowItems')
	pref_new = dict()
	for i in pref_as_is:
		if i == "Bitmap":
			global bitmapWasOn
			bitmapWasOn = pref_as_is[i]
			pref_new[i] = bitmap
		elif i == "Grid":
			global gridWasOn
			gridWasOn = pref_as_is[i]
			pref_new[i] = grid
		else:
			pref_new[i] = pref_as_is[i]
	setDefault('glyphZoomViewShowItems', pref_new)
	PostNotification("doodle.preferencesChanged")

class BitmapCorps(object):
	def __init__(self):
		width = 100
		height = 300
		margin = 7

		self.w = FloatingWindow((width, height), "?")
		self.w.grid8 = SquareButton((margin,next(0),-margin,atHeight), '8', callback=self.getGrid, sizeStyle='small')
		self.w.grid9 = SquareButton((margin,next(1),-margin,atHeight), '9', callback=self.getGrid, sizeStyle='small')
		self.w.grid10 = SquareButton((margin,next(2),-margin,atHeight), '10', callback=self.getGrid, sizeStyle='small')
		self.w.grid11 = SquareButton((margin,next(3),-margin,atHeight), '11', callback=self.getGrid, sizeStyle='small')
		self.w.grid12 = SquareButton((margin,next(4),-margin,atHeight), '12', callback=self.getGrid, sizeStyle='small')
		self.w.grid13 = SquareButton((margin,next(5),-margin,atHeight), '13', callback=self.getGrid, sizeStyle='small')
		self.w.grid14 = SquareButton((margin,next(6),-margin,atHeight), '14', callback=self.getGrid, sizeStyle='small')
		self.w.grid15 = SquareButton((margin,next(7),-margin,atHeight), '15', callback=self.getGrid, sizeStyle='small')
		self.w.grid16 = SquareButton((margin,next(8),-margin,atHeight), '16', callback=self.getGrid, sizeStyle='small')
		self.w.grid17 = SquareButton((margin,next(9),-margin,atHeight), '17', callback=self.getGrid, sizeStyle='small')
		self.w.grid18 = SquareButton((margin,next(10),-margin,atHeight), '18', callback=self.getGrid, sizeStyle='small')
		self.w.customTxt = TextBox((margin,next(11.5),-margin,atHeight), 'custom corps', sizeStyle='mini')
		self.w.customEdit = EditText((margin, next(12.3), -40, atHeight))
		self.w.customButton = SquareButton((60, next(12.3), -margin, atHeight),'go',sizeStyle='small', callback=self.customCorps)
		
		self.w.bind("close",self.keepGridAndBitmap)

		self.w.open()
		showGridAndBitmap()

	def getGrid(self,sender):
		f = CurrentFont()
		g = CurrentGlyph()
		grid = f.info.unitsPerEm / int(sender.getTitle())
		setGrid(grid)
		self.w.setTitle(str(sender.getTitle()))

	def customCorps(self,sender):
		try:
			int(self.w.customEdit.get())
		except:
			ValueError()
		f = CurrentFont()
		g = CurrentGlyph()
		grid = f.info.unitsPerEm / int(self.w.customEdit.get())
		setGrid(grid)
		self.w.setTitle(str(self.w.customEdit.get()))
		


	def keepGridAndBitmap(self, sender):
		showGridAndBitmap(grid=gridWasOn, bitmap=bitmapWasOn)


BitmapCorps()


