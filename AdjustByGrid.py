"""
Adjust by Grid
Adjust by Grid is a part of Screen Font Pack

Work with the left- and rightMargin
and with the points in the glyph

TODO: 
	adjust leftMargin: scroll the GlyphView

Thom Janssen 2014
"""

from vanilla import * 
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from mojo.UI import CurrentGlyphWindow

from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *

event = "draw" 

def showGrid(grid=True):
	pref_as_is = getDefault('glyphZoomViewShowItems')
	pref_new = dict()
	for i in pref_as_is:
		if i == "Grid":
			global gridWasOn
			gridWasOn = pref_as_is[i]
			pref_new[i] = grid
		else:
			pref_new[i] = pref_as_is[i]
	setDefault('glyphZoomViewShowItems', pref_new)
	PostNotification("doodle.preferencesChanged")

class AdjustByGrid(object):
	def __init__(self):
		width = 134
		height = 127
		margin = 7
		self.w = FloatingWindow((width, height), "")
		self.w.margin = TextBox((margin, 5, -0, 15), 'margins', sizeStyle = 'mini')
		self.w.points = TextBox((margin, 65, -0, 15), 'points', sizeStyle = 'mini')		

		self.w.glyphWidthPlusLeftGrid = SquareButton((margin,20, 40, 20), '+', callback=self.glyphWidthPlusMinGridOnLeft, sizeStyle = "small")
		self.w.glyphWidthMinLeftGrid = SquareButton((margin,40, 40, 20), '-', callback=self.glyphWidthPlusMinGridOnLeft, sizeStyle = "small")
		self.w.glyphWidthOnGrid = SquareButton((margin+40,20, 40, 20), '=', callback=self.roundGrid, sizeStyle = "small")
		self.w.glyphCenter = SquareButton((margin+40,40, 40, 20), 'c', callback=self.center, sizeStyle = "small")
		self.w.glyphWidthPlusRightGrid = SquareButton((margin+80,20,40, 20), '+', callback=self.plusGrid, sizeStyle = "small")
		self.w.glyphWidthMinRightGrid = SquareButton((margin+80,40,40, 20), '-', callback=self.minGrid, sizeStyle = "small")

		self.w.moveUp = SquareButton((margin+40,80,40,20), unichr(8593), callback=self.moveUp, sizeStyle = "small")
		self.w.moveLeft = SquareButton((margin,80,40,40), unichr(8592), callback=self.moveLeft, sizeStyle = "small")
		self.w.moveRight = SquareButton((margin+80,80,40,40), unichr(8594), callback=self.moveRight, sizeStyle = "small")
		self.w.moveDown = SquareButton((margin+40,100,40,20), unichr(8595), callback=self.moveDown, sizeStyle = "small")
		
		self.w.bind("close",self._close)
		addObserver(self, "observerWidthGrid", event)

		self.w.open()
		showGrid(grid=True)
		
	def _close(self, sender):
		removeObserver(self, event)
		showGrid(grid=gridWasOn)
	
	def _getGrid(self):
		gridX = getDefault('glyphViewGridx')
		gridY = getDefault('glyphViewGridy')
		return (gridX, gridY)	
	
	def observerWidthGrid(self, info):
		grid = getDefault('glyphViewGridx')
		fill(0,.5,.2,.9)
		font("Menlo", 24)
		gridUnits = str(round(info["glyph"].width / grid ,3))
		if gridUnits.split(".")[1] == '0':
			gridUnits = gridUnits.split(".")[0]
		text(gridUnits,(info["glyph"].width+(int(grid)/12)), 0)


	def glyphWidthPlusMinGridOnLeft(self,sender):
		g=CurrentGlyph()
		(gridX, gridY) = self._getGrid()
		g.prepareUndo('gWidthLeft')
		if sender.getNSButton().title() == "+":
			g.width += getDefault('glyphViewGridx')
			g.move((getDefault('glyphViewGridx'),0))
			gw = CurrentGlyphWindow()
			view = gw.getGlyphView()
			offset = view.offset()
			(x,y) = offset
			x-=gridX
			view.setOffset((x,y))
			view.updateGlyphView_(None)
		if sender.getNSButton().title() == "-":
			g.width -= getDefault('glyphViewGridx')
			g.move((-getDefault('glyphViewGridx'),0))
			gw = CurrentGlyphWindow()
			view = gw.getGlyphView()
			offset = view.offset()
			(x,y) = offset
			x+=gridX
			view.setOffset((x,y))
			view.updateGlyphView_(None)
		g.performUndo()
		g.update()

	def minGrid(self,sender):
		g=CurrentGlyph()
		g.prepareUndo('minGrid')
		g.width -= getDefault('glyphViewGridx')
		g.performUndo()
		g.update()
	
	def plusGrid(self,sender):
		g=CurrentGlyph()
		g.prepareUndo('plusGrid')
		g.width += getDefault('glyphViewGridx')
		g.performUndo()
		g.update()
	
	def roundGrid(self, sender):
		g=CurrentGlyph()
		g.prepareUndo('roundGrid')

		grid = getDefault('glyphViewGridx')
		g.width = int(round(g.width/grid)*grid)
		g.performUndo()
		g.update()
	
	def moveUp(self, sender):
		(gridX, gridY) = self._getGrid()
		g=CurrentGlyph()
		g.prepareUndo()
		for contour in g:
			for bPoint in contour.bPoints:
				if bPoint.selected:
					anchor = bPoint.anchor
					bcpIn = bPoint.bcpIn
					bcpOut = bPoint.bcpOut
			
					bPoint.anchor = anchor[0], anchor[1] + gridY
					if bcpIn:
				# if there is a bcpIn just reset it, so it get updated relatively to the anchor
						bPoint.bcpIn = bcpIn	
					if bcpOut:
				# if there is a bcpOut just reset it, so it get updated relatively to the anchor
						bPoint.bcpOut = bcpOut
		g.update()
		g.performUndo()

	def moveLeft(self, sender):
		(gridX, gridY) = self._getGrid()
		g=CurrentGlyph()
		g.prepareUndo()
		for contour in g:
			for bPoint in contour.bPoints:
				if bPoint.selected:
					anchor = bPoint.anchor
					bcpIn = bPoint.bcpIn
					bcpOut = bPoint.bcpOut
					bPoint.anchor = anchor[0] - gridX, anchor[1]
					if bcpIn:
				# if there is a bcpIn just reset it, so it get updated relatively to the anchor
						bPoint.bcpIn = bcpIn	
					if bcpOut:
				# if there is a bcpOut just reset it, so it get updated relatively to the anchor
						bPoint.bcpOut = bcpOut
		g.update()
		g.performUndo()

	def moveRight(self, sender):
		(gridX, gridY) = self._getGrid()
		g=CurrentGlyph()
		g.prepareUndo()
		for contour in g:
			for bPoint in contour.bPoints:
				if bPoint.selected:
					anchor = bPoint.anchor
					bcpIn = bPoint.bcpIn
					bcpOut = bPoint.bcpOut
					bPoint.anchor = anchor[0] + gridX, anchor[1]
					if bcpIn:
				# if there is a bcpIn just reset it, so it get updated relatively to the anchor
						bPoint.bcpIn = bcpIn	
					if bcpOut:
				# if there is a bcpOut just reset it, so it get updated relatively to the anchor
						bPoint.bcpOut = bcpOut
		g.update()	
		g.performUndo()
	
	def moveDown(self, sender):
		(gridX, gridY) = self._getGrid()
		g=CurrentGlyph()
		g.prepareUndo()
		for contour in g:
			for bPoint in contour.bPoints:
				if bPoint.selected:
					anchor = bPoint.anchor
					bcpIn = bPoint.bcpIn
					bcpOut = bPoint.bcpOut
					bPoint.anchor = anchor[0], anchor[1] - gridY
					if bcpIn:
				# if there is a bcpIn just reset it, so it get updated relatively to the anchor
						bPoint.bcpIn = bcpIn	
					if bcpOut:
				# if there is a bcpOut just reset it, so it get updated relatively to the anchor
						bPoint.bcpOut = bcpOut
		g.update()
		g.performUndo()
	
	def center(self, sender):
		g = CurrentGlyph()
		margin = (g.width-(g.box[2]-g.box[0]))/2
		g.prepareUndo()
		g.leftMargin, g.rightMargin = (margin, margin)			
		g.update()
		
AdjustByGrid()
