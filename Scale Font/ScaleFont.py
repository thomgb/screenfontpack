"""
Scale Font
Scale Font is a part of the Screen Font Pack

scales a font down by the upm
incl: blue, guides, components, ...

TODO: 

e.g.
512 upm on a 32 units grid = 16pt font


Thom Janssen 2014
"""

from vanilla import *
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from math import ceil, floor

f = CurrentFont()
g = CurrentGlyph()

attrHeight = 22

upm = f.info.unitsPerEm
asc = f.info.ascender
cap = f.info.capHeight
xhe = f.info.xHeight
des = f.info.descender

def next(y):
	return y*attrHeight + 10

def isEven(number):
	return number % 2 == 0

def roundGlyphWidth(x, grid=32, scale=0): ##scale 0=nearest, 1=up, 2=down
	if scale == 0:  
		return int(round(x/grid)*grid)
	if scale == 1:
		return int(round(ceil(x/grid))*grid)
	if scale == 2:
		return int(round(floor(x/grid))*grid)

class ScaleFont(object):
		
	def __init__(self):
		width = 240
		height = 300
		
		self.w = FloatingWindow((width, height), "Scale Font")
		
		self.w.currentUPMtxt = TextBox((10,next(0),-10,attrHeight), "Current UPM: %s" % int(upm))
		
		self.w.newUPMtxt = TextBox((10,next(1.5),-10,attrHeight), "New UPM", )		
		self.w.newUPM = EditText((10,next(2.5), -10, attrHeight), text="512")
		
		self.w.adjustGridCheckbox = CheckBox((10,next(4),-10,attrHeight),"adjust grid", sizeStyle = 'small', callback=self.checkAjustGrid)
		self.w.adjustGridCheckbox.set(1)
		self.w.grid = EditText((10,next(5), -10, attrHeight),text="32")
		
		self.w.roundGlyphWidthOnGrid = CheckBox((10,next(6.5), -10, attrHeight),"round glyph width on grid", sizeStyle = 'small', callback=self.checkRoundGlyph)
		self.w.howRoundGlypgWidth = RadioGroup((10,next(7.5), -10, attrHeight), ["nearest", "up", "down"], sizeStyle = 'small',isVertical=0)
		self.w.howRoundGlypgWidth.set(0)
		self.w.howRoundGlypgWidth.enable(0)
		self.w.scaleGlyphToNewWidth = CheckBox((10,next(8.5), -10, attrHeight),"adjust contours to new width", sizeStyle = 'small')
		self.w.scaleGlyphToNewWidth.enable(0)

		self.w.roundKerning = CheckBox((10, next(10),-10, attrHeight),"round kerning", sizeStyle="small")

		self.w.ok = Button((10,next(12), -10, attrHeight), "scale", callback=self.scaleFont) 

		self.w.spinner = ProgressSpinner((180, 10, 32, 32), displayWhenStopped=False)
		self.w.open()
		
	def checkAjustGrid(self, sender):
		self.w.grid.enable(sender.get())

	def checkRoundGlyph(self, sender):
		self.w.howRoundGlypgWidth.enable(sender.get())
		self.w.scaleGlyphToNewWidth.enable(sender.get())
	
	def scaleFont(self, sender):
		self.w.spinner.start()
		newUnitsPerEm = int(self.w.newUPM.get())
		grid = int(self.w.grid.get())
		
		scaleFactor = newUnitsPerEm / upm

		f.info.unitsPerEm = int(upm * scaleFactor)
		f.info.ascender = int(asc * scaleFactor)
		f.info.capHeight = int(cap * scaleFactor)
		f.info.xHeight = int(xhe * scaleFactor)
		f.info.descender = int(des * scaleFactor)
		
		for g in f:
			#if g.selected:
				g.prepareUndo()
				g.scale((scaleFactor, scaleFactor))

				for c in g.components:
					x = (c.offset[0]*scaleFactor) - c.offset[0]
 					y = (c.offset[1]*scaleFactor) - c.offset[1]
					c.move((x,y))

				for guide in g.guides:
					guide.x = guide.x * scaleFactor
					guide.y = guide.y * scaleFactor
					
				g.width = int(g.width * scaleFactor)
				g.update()
				g.performUndo()
			
		allBlues = [
			f.info.postscriptFamilyOtherBlues,
			f.info.postscriptBlueFuzz,
			f.info.postscriptBlueScale,
			f.info.postscriptBlueValues,
			f.info.postscriptOtherBlues,
			f.info.postscriptBlueShift,
			]
		for aBlue in allBlues:
			try:
				n = 0
				for blue in range(len(aBlue)):
					if isEven(n):
						aBlue[blue] =  int(floor(aBlue[blue] * scaleFactor))
					else:
						aBlue[blue] =  int(ceil(aBlue[blue] * scaleFactor))
					n+=1
			except:
				pass
		
		if self.w.adjustGridCheckbox.get():
			setDefault('glyphViewGridx', grid)
			setDefault('glyphViewGridy', grid)
			PostNotification("doodle.preferencesChanged")
		
			if self.w.roundGlyphWidthOnGrid.get():
				for g in f:
					#if g.selected:
					g.prepareUndo("roundGlyphWidth")
					roundGlyphWidthValues = roundGlyphWidth(g.width, grid, self.w.howRoundGlypgWidth.get())
					scaleFactor = roundGlyphWidthValues/g.width
					g.width = roundGlyphWidthValues
					if self.w.scaleGlyphToNewWidth.get():
						g.scale((scaleFactor,1))
					g.performUndo()
		if self.w.roundKerning.get():
			k = f.kerning
			for kernPair in k.keys():
				k[kernPair] = int(round(k[kernPair]/grid)*grid)
		self.w.spinner.stop()
ScaleFont()